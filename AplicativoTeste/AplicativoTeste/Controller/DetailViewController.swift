//
//  DetailViewController.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController:UIViewController{
    var empresa:Empresa?
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?,empresa:Empresa) {
        super.init(nibName: nil, bundle: nil)
        self.empresa = empresa
        self.view = DetailView(frame: self.accessibilityFrame, empresa: empresa)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = false
        self.navigationItem.title = empresa?.name
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController!.navigationBar.tintColor = UIColor.white;
        let editButton   = UIBarButtonItem(barButtonSystemItem: .search, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = editButton
        
    }

    @objc func comeBackToList(){
        self.navigationController?.popViewController(animated: false)
    }
}
