//
//  ViewController.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let loginView = LoginView(frame: self.accessibilityFrame)
        self.view = loginView
        setListener()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func setListener(){
        NotificationCenter.default.addObserver(self, selector: #selector(login), name: Notification.loginAccepted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginRejected), name: Notification.loginRejected, object: nil)
    }
    
    @objc func loginRejected(){
        DispatchQueue.main.async {
             let alert = UIAlertController(title: "Falha", message: "E-mail ou senha errados.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: false)
        }
        
    }
    
    @objc func login(){
        DispatchQueue.main.async {
            self.navigationController?.show(HomeViewController(), sender: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    
    }
}

