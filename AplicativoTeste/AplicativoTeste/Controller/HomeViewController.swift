//
//  HomeViewController.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController:UIViewController{
    let homeView = HomeView()
    let searchBar = UISearchBar()
    var firstTimeInView = true
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nil, bundle: nil)
        self.view = homeView
        self.title = ""
        setNotificationObservers()
    }
    
    func setNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(pushDetailPage), name: Notification.openDetailPage, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(!firstTimeInView) {showSearchBar()}
    }
    @objc func pushDetailPage(notification:Notification){
        firstTimeInView = false
        let detailViewController = DetailViewController(nibName: nil, bundle: nil, empresa: notification.userInfo?["empresa"] as! Empresa)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewWillAppear(_ animated: Bool) {
        loadNormalNavigationBar()
    }
    func loadNormalNavigationBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = true
        let titleImageView = UIImageView(image: UIImage(named: "logoNav"))
               
        self.navigationItem.titleView = titleImageView
        
        let item = UIBarButtonItem(barButtonSystemItem: .search , target: self, action: #selector(showSearchBar))
        item.tintColor = .white
        self.navigationItem.rightBarButtonItem = item
        self.homeView.tableEmpresas.isHidden = true
    }
    @objc func showSearchBar(){
        homeView.middleLabel.isHidden = true
        searchBar.delegate = self
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self] ).tintColor = UIColor.white
        searchBar.searchTextField.backgroundColor = .white
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Pesquisar"
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = nil
        self.homeView.tableEmpresas.isHidden = false

    }
    
}

extension HomeViewController:UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        loadNormalNavigationBar()
        homeView.middleLabel.isHidden = false
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.homeView.dataSource.filter = searchText
        self.homeView.tableEmpresas.reloadData()
    }
}
