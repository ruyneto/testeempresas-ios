//
//  NavigationController.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class RootNavigationController:UINavigationController{
    override init(rootViewController:UIViewController){
        super.init(rootViewController: rootViewController)
        
        personalizeNavigationBar()
    }
    
    func personalizeNavigationBar(){
        setNavigationBarHidden(true, animated: true)
          self.navigationBar.barTintColor = #colorLiteral(red: 0.8705882353, green: 0.2784313725, blue: 0.4470588235, alpha: 1)
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
