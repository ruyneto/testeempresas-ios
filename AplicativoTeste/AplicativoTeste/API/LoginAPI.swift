//
//  LoginAPI.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation


class LoginAPI:APIRequest{
    @discardableResult
    static func loginWith(username: String, password: String, callback: ResponseBlock<User>?) -> AuthenticationAPI {
        
        let request = AuthenticationAPI(method: .post, path: "/api/v1/users/auth/sign_in", parameters: ["email": username, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let user = User(dictionary: response)
                callback!(user, nil, cache)

            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()

        return request
    }
}
