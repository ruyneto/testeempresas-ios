//
//  LoginService.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 08/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation


class LoginService{
    
    
    private static func callBackLogin(data:Data?,response:URLResponse?,erro:Error?){
        if((erro) != nil) {
            print(erro as Any)
            return
        }else{
            let status = (response as! HTTPURLResponse).statusCode
            if(status==200){
                let client      = (response as! HTTPURLResponse).allHeaderFields["client"]!
                let acessToken  = (response as! HTTPURLResponse).allHeaderFields["access-token"]!
                let uid         = (response as! HTTPURLResponse).allHeaderFields["uid"]!
                let userDefault = UserDefaults()
                userDefault.setValue(acessToken, forKey: "access-token")
                userDefault.setValue(client, forKey: "client")
                userDefault.set(uid, forKey: "uid")
                NotificationCenter.default.post(name: Notification.loginAccepted, object: nil)
            }else{
                NotificationCenter.default.post(name: Notification.loginRejected, object: nil)
            }
            
        }
    }
    static func login(username:String,password:String){
        guard let url = URL(string: "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in") else { return  }
        do{
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let json = ["email":username,"password":password]
            let data = try JSONSerialization.data(withJSONObject: json, options: [])
            request.httpBody = data
            request.httpMethod = "POST"
            URLSession.shared.dataTask(with: request, completionHandler: callBackLogin ).resume()
        }catch{
        }
    }
}
