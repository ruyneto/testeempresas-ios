//
//  HomeTableDelegate.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class HomeTableDataSource:UIViewController, UITableViewDataSource,UITableViewDelegate{
    
    let data = [
        Empresa(data: UIImage(named: "imgE1")!,name: "Maquinas Para Todos", description: "Olha só minhas maquinas"),
        Empresa(data: UIImage(named: "imgE1")!,name: "Computadores SA", description: "Computadores Black Friday"),
        Empresa(data: UIImage(named: "imgE1")!,name: "Baratao da Carne", description: "Carnes Baratas"),
        Empresa(data: UIImage(named: "imgE1")!,name: "Aeroporto de Belo Horizonte", description: "Muito legal")]
    var filter:String = ""
    var filteredData:[Empresa]{
        get{
            if(filter.count==0){
                return data
            }else{
                return data.filter{$0.name.lowercased().contains(filter.lowercased())}
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.5529411765, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
        cell.imageView?.image = filteredData[indexPath.row].imageData
        cell.textLabel!.text = filteredData[indexPath.row].name
        cell.detailTextLabel?.text = filteredData[indexPath.row].description
        cell.detailTextLabel?.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
 NotificationCenter.default.post(name: Notification.openDetailPage, object: nil, userInfo: ["empresa":filteredData[indexPath.row]])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
