//
//  Empresa.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class Empresa{
    var imageData   : UIImage
    var name        : String
    var description : String
    init(data:UIImage,name:String,description:String){
        self.imageData   = data
        self.name        = name
        self.description = description
    }
}
