//
//  DetailView.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class DetailView:UIView{
    var empresa:Empresa?
    init(frame: CGRect,empresa:Empresa) {
        super.init(frame: frame)
        self.empresa = empresa
        setSubViews()
        self.backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setSubViews(){
        setTopImage()
        setDescriptionLabel()
    }

    func setDescriptionLabel(){
        let textDescription = UILabel()
        textDescription.numberOfLines = 0
        textDescription.text = empresa?.description
        self.addSubview(textDescription)
        ConstraintFactory.DetailPage.descriptionText(parentView: self, childView: textDescription)
        StyleFactory.DetailView.description(view: textDescription)
    }
    func setTopImage(){
        let image = UIImage(named: "imgE1")
        let viewImage = UIImageView(image: image)
        self.addSubview(viewImage)
        ConstraintFactory.DetailPage.topImage(parentView: self, childView: viewImage)
    }
}
