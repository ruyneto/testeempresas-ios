//
//  LoginView.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class LoginView:UIView{
    let passwordField = UITextField()
    let emailField = UITextField()
    let entrarButton = UIButton()
    var alreadyTriedLogin = false
    override init(frame: CGRect) {
        super.init(frame:frame)
        setSubviews()
        setConfiguration()
        setListeners()
        setObservers()
        //Teste
        emailField.text    = "testeapple@ioasys.com.br"
        passwordField.text = "12341234"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(freeLogin), name: Notification.loginAccepted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(freeLogin), name: Notification.loginRejected, object: nil)
    }
    
    @objc func freeLogin(){
        alreadyTriedLogin = false
    }
    
    func setListeners(){
        entrarButton.addTarget(self, action: #selector(executeLogin), for: .touchUpInside)
        
    }

    @objc func executeLogin(){
        if(!alreadyTriedLogin){
           LoginService.login(username: self.emailField.text!, password: passwordField.text!)
            alreadyTriedLogin = true
        }
    }
    
    func setConfiguration(){
        self.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9137254902, blue: 0.8431372549, alpha: 1)
    }
    
    func setSubviews(){
        setLogoImage()
        setTitleLabel()
        setSubTitleLabel()
        setEmailTextField()
        setSenhaTextField()
        setButtonEntrar()
    }
    func setButtonEntrar(){
        entrarButton.setTitle("ENTRAR", for: .normal)
        entrarButton.setTitleColor(.white, for: .normal)
        entrarButton.backgroundColor = #colorLiteral(red: 0.3411764706, green: 0.7333333333, blue: 0.737254902, alpha: 1)
        self.addSubview(entrarButton)
        ConstraintFactory.LoginPage.entrarButtonConstrat(parentView: self, childView: entrarButton)
        StyleFactory.LoginView.button(button: entrarButton)
        
    }
    func setSenhaTextField(){
        let lockerIcon  = UIImage(named: "icCadeado")
        passwordField.isSecureTextEntry = true
        passwordField.placeholder = "Senha"
        passwordField.leftView    = UIImageView(image: lockerIcon)
        passwordField.leftViewMode = .always
        self.addSubview(passwordField)
        ConstraintFactory.LoginPage.passwordFieldConstrat(parentView: self, childView: passwordField)
        StyleFactory.LoginView.formField(view: passwordField)
    }
    func setEmailTextField(){
        let emailIcon  = UIImage(named: "icEmail")
        emailField.placeholder = "Email"
        emailField.leftView    = UIImageView(image: emailIcon)
        emailField.leftViewMode = .always
        self.addSubview(emailField)
        ConstraintFactory.LoginPage.emailFieldConstrat(parentView: self, childView: emailField)
        StyleFactory.LoginView.formField(view: emailField)
    }
    
    func setSubTitleLabel(){
        let subtitleLabel = UILabel()
        subtitleLabel.text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textAlignment = .center
        self.addSubview(subtitleLabel)
        ConstraintFactory.LoginPage.subtitleLabelConstrat(parentView: self, childView: subtitleLabel)
    }
    func setTitleLabel(){
        let titleLabel = UILabel()
        titleLabel.text = "BEM-VINDO AO EMPRESAS"
        titleLabel.font = UIFont(name: "Roboto-Bold", size: 16.0)
        self.addSubview(titleLabel)
        ConstraintFactory.LoginPage.titleLabelConstrat(parentView: self, childView: titleLabel)

    }
    
    func setLogoImage(){
        let image = UIImageView(image: UIImage(named: "logoHome"))
        self.addSubview(image)
        ConstraintFactory.LoginPage.titleImageConstrat(parentView: self, childView: image)
    }

}
