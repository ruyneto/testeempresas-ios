//
//  HomeView.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

class HomeView:UIView{
    let dataSource    = HomeTableDataSource()
    let middleLabel   = UILabel()
    let tableEmpresas = UITableView()
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9137254902, blue: 0.8431372549, alpha: 1)
        setSubviews()
    }
    
    func setSubviews(){
        setMiddleLabel()
        setTableView()
    }
    
    
    func setMiddleLabel(){
        middleLabel.text = "Clique na busca para iniciar."
        self.addSubview(middleLabel)
        ConstraintFactory.HomePage.middleLabelConstraint(parentView: self, childView: middleLabel)
    }
    
    func setTableView(){
        tableEmpresas.isHidden = true
        tableEmpresas.tableFooterView = UIView()
        tableEmpresas.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableEmpresas.dataSource = dataSource
        tableEmpresas.delegate   = dataSource
        tableEmpresas.reloadData()
        self.addSubview(tableEmpresas)
        ConstraintFactory.HomePage.tableEmpresasConstraint(parentView: self, childView: tableEmpresas)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
