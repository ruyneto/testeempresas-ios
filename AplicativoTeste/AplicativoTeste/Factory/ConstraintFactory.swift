//
//  ConstrantFactory.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

struct ConstraintFactory{
    struct LoginPage{
        static func titleImageConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 69).isActive = true
        }
        static func titleLabelConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 150).isActive = true
        }
        static func subtitleLabelConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 180).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 37).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: -37).isActive = true
        }
        static func emailFieldConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 316).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 37).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: -37).isActive = true
        }
        
        static func passwordFieldConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 380).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 37).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: -37).isActive = true
        }
        
        static func entrarButtonConstrat(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: -37).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 37).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: -37).isActive = true
            childView.heightAnchor.constraint(equalToConstant: 52.0).isActive = true
        }
    }
    
    struct HomePage{
        static func middleLabelConstraint(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
            childView.centerYAnchor.constraint(equalTo: parentView.centerYAnchor).isActive = true
        }
        static func tableEmpresasConstraint(parentView:UIView,childView:UITableView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 9).isActive = true
            childView.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 0).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 0).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: 0).isActive = true
        }
    }
    struct DetailPage{
        static func topImage(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 60).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 0).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: 0).isActive = true
        }
        static func descriptionText(parentView:UIView,childView:UIView){
            childView.translatesAutoresizingMaskIntoConstraints = false
            childView.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 200).isActive = true
            childView.leftAnchor.constraint(equalTo: parentView.leftAnchor, constant: 18.5).isActive = true
            childView.rightAnchor.constraint(equalTo: parentView.rightAnchor, constant: -18.5).isActive = true
            childView.heightAnchor.constraint(equalToConstant: 375).isActive = true;
        }
    }
}
