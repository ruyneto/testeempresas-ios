//
//  StyleFactory.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 08/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

struct StyleFactory{
    struct LoginView{
        static func formField(view:UITextField){
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: 30, width: 300, height: 1.0)
            bottomLine.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.2235294118, blue: 0.2352941176, alpha: 1)
            view.borderStyle = UITextField.BorderStyle.none
            view.layer.addSublayer(bottomLine)
        }
        static func button(button:UIButton){
            button.layer.cornerRadius = 6
            button.titleLabel!.font    = UIFont(name: "Roboto-Bold", size: 20)
        }
    }
    struct DetailView{
        static func description(view:UILabel){
            view.textColor = #colorLiteral(red: 0.5529411765, green: 0.5490196078, blue: 0.5490196078, alpha: 1)
        }
    }
}
