//
//  Notifications.swift
//  AplicativoTeste
//
//  Created by Ruy de Ascencão Neto on 07/12/19.
//  Copyright © 2019 Academy. All rights reserved.
//

import Foundation
import UIKit

extension Notification{
    private static let nameProject = "aplicativoteste"
    static let openDetailPage = Notification.Name("\(nameProject).openDetailPage")
    static let loginAccepted = Notification.Name("\(nameProject).loginAccepted")
    static let loginRejected = Notification.Name("\(nameProject).loginRejected")
}
